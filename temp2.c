void HAL_RTC_AlarmIRQHandler(RTC_HandleTypeDef *hrtc)
{
  /* Get interrupt status */
  uint32_t tmp = READ_REG(RTC->SMISR);

  if ((tmp & RTC_SMISR_ALRAMF) != 0u)
  {
    /* Clear the AlarmA interrupt pending bit */
    WRITE_REG(RTC->SCR, RTC_SCR_CALRAF);
#if (USE_HAL_RTC_REGISTER_CALLBACKS == 1)
    /* Call Compare Match registered Callback */
    hrtc->AlarmAEventCallback(hrtc);
#else
    HAL_RTC_AlarmAEventCallback(hrtc);
#endif
  }

  if ((tmp & RTC_SMISR_ALRBMF) != 0u)
  {
    /* Clear the AlarmB interrupt pending bit */
    WRITE_REG(RTC->SCR, RTC_SCR_CALRBF);
#if (USE_HAL_RTC_REGISTER_CALLBACKS == 1)
    /* Call Compare Match registered Callback */
    hrtc->AlarmBEventCallback(hrtc);
#else
    HAL_RTCEx_AlarmBEventCallback(hrtc);
#endif

  }

  /* Change RTC state */
  hrtc->State = HAL_RTC_STATE_READY;
}
