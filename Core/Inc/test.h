#include <main.h>
#include <stdint.h>
#include <stdbool.h>

//void JoinNetwork( void );
//extern void main_lora_func(void);





///*


//Frame Header Control Byte for FrameHeader
typedef struct FrameHeaderControlByte
{
	uint8_t ADR 			: 1;	//Adaptive Data Rate bit
	uint8_t ADRACKreq		: 1;	//ADR acknowledgment request
	uint8_t ACK 			: 1;	//acknowledge bit
	uint8_t ClassB 			: 1;	//
	uint8_t FoptsLen 		: 4;	//Length of  Frame options field
}Fctrl_t;

//frameheader for MAC payload
typedef struct FHDR
{
	uint8_t devAddr[4];	//Device address
	Fctrl_t Fctrl;		//
	uint8_t Fcnt[2];	//Frame counter
	uint8_t Fopts[15]; 	//Used for MAC commands
}FDHR_t;

typedef struct MACpayload
{

	FDHR_t FHDR;		//frameheader for MAC payload
	uint8_t Fport;			//Port field, must be set when payload is present
	uint8_t* FRMpayload;	//pointer to the payload
}MACpayload_t;

typedef enum LMAC_MajorVersion
{
	LoRaWAN_R1,
	//RFU
	//RFU,
	//RFU,
}Major_t;

typedef enum LMAC_Mtype
{
    JOIN_REQUEST,
    JOIN_ACCEPT,
    UNCONFIRMED_DATA_UP,
	UNCONFIRMED_DATA_DOWN,
	CONFIRMED_DATA_UP,
	CONFIRMED_DATA_DOWN,
    //RFU,
    PROPRIETARY = 7,
}Mtype_t;

typedef struct MACHeaderByte
{
	Mtype_t Mtype	: 3; //message type
	uint8_t RFU		: 3; //not in use
	Major_t Major	: 2; //describes the version of LoRa used
}MHDR_t;

typedef struct LoRaPacket
{
    MHDR_t MHDR;				//MAC Header
    MACpayload_t MACpayload;	//Pointer to the payload
    uint8_t MIC[4];				//Message Integrity code
}LoRaPacket_t;

void configPacket(int packettype, LoRaPacket_t* packet, uint8_t* p_payload);

void makeStringMessage( LoRaPacket_t* packet, int payload_len, uint8_t* message);

void calculateMIC(uint8_t* PHY, LoRaPacket_t* packet, uint8_t payload_len);

void makeJoinMsg(LoRaPacket_t * packet, uint8_t* PHY);

void joinNetwork(void);

int	snoopLoRa(void);

void sem_join(void);
//Description: Configures a uint8_t string by placing the given data in one big array
//arg - uint8_t* string: pointer to datastream
//arg - uint8_t num_args: total number of arguments in (...)
//arg - (...) arguments in here should come in pairs-> 	first: 	length of data
//														second:	pointer to the data
void strConfig(uint8_t* string, uint8_t num_args, ...);

void strConfig2(uint8_t* str, int argCnt, uint8_t* datap_list[], int* lenp_list);
