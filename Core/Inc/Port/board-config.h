/*!
 * \file      board-config.h
 *
 * \brief     Board configuration
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 *               ___ _____ _   ___ _  _____ ___  ___  ___ ___
 *              / __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
 *              \__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
 *              |___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
 *              embedded.connectivity.solutions===============
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 *
 * \author    Daniel Jaeckle ( STACKFORCE )
 *
 * \author    Johannes Bruder ( STACKFORCE )
 */
#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * Defines the time required for the TCXO to wakeup [ms].
 */
#if defined( SX1262MBXDAS )
#define BOARD_TCXO_WAKEUP_TIME                      5
#else
#define BOARD_TCXO_WAKEUP_TIME                      0
#endif


/*!
 * Board MCU pins definitions
 */
#define RADIO_RESET                                 PA_3		//May be reassigned

#define RADIO_MOSI                                  PA_7		//Cannot be reassigned
#define RADIO_MISO                                  PA_6		//Cannot be reassigned
#define RADIO_SCLK                                  PA_5		//Cannot be reassigned

#define RADIO_NSS                                   PF_13		//Must be reassigned
#define RADIO_BUSY                                  PE_13		//Must be reassigned
#define RADIO_DIO_1                                 PE_11		//Must be reassigned

#define RADIO_ANT_SWITCH_POWER                      PF_12		//Must be reassigned

#define LED_1	/*Green*/                           PC_7		//Is not mandatory to use
#define LED_2	/*Blue*/                            PB_7		//Is not mandatory to use
#define LED_3	/*Red*/                             PA_9		//Is not mandatory to use

/* possible adc channels and their pins*/
#define PT1000_ADC_CHANNEL							1
#define PT1000_ADC_PIN								PC_0			//channel 1	// must be changed to one of the other channels (PC_0 is not available on LQFP48 package)
//#define PT1000_ADC_PIN								PA_0			//channel 5
//#define PT1000_ADC_PIN								PA_1			//channel 6
//#define PT1000_ADC_PIN								PA_2			//channel 7
//#define PT1000_ADC_PIN								PA_3			//channel 8
//#define PT1000_ADC_PIN								PA_4			//channel 9
//#define PT1000_ADC_PIN								PB_0			//channel 15
//#define PT1000_ADC_PIN								PB_1			//channel 16


#ifdef __cplusplus
}
#endif

#endif // __BOARD_CONFIG_H__
