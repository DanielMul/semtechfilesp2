#include "systime.h"
#define STANDARD_SAMPLE_TIME 	5000
#define	STANDARD_TEMP_OFFSET 	0.0
#define STANDARD_MAX_SAMPLE_TIME 20000
#define STANDARD_SYSTEM_TIMESTAMP 0x5F6379D0

/*
 * struct with information on the sensor
 */
typedef struct sensor_config
{
	int sample_time;
	float temp_offset;
	int max_sample_time;
	SysTime_t timestamp;
}sensor_config_t;

/*
 * enum with types for downlink config
 */
enum update_types_config
{
	sample_time_1s 		= 0x01,
	sample_time_30s 	= 0x02,
	sample_time_5m 		= 0x04,
	sample_offset_50mK 	= 0x08,
	sample_offset_1K	= 0x10,
	sample_time_max_30s	= 0x20,
	sample_time_max_5m	= 0x40,
	update_system_time	= 0x80,
}setting_conf_t;

/*sensorProcess
 * Main process for the sensor gets data and checks if it needs to be sent
 *Gets the sensorvalue translates it to temp
 *Checks last time message sent
 *Checks last sent Temperature
 *Inputs:	sensor_config_t* config) -> pointer to configuration structure
 *Returns:	1 or 0 depinding on last time and Temp
 */
uint8_t sensorProcess(sensor_config_t* sensor);

/*sensorSettingsUpdate
 *Adds the temperature to the format
 *Inputs:	uint8_t *dest -> pointer to place in payloads tring
 *Returns:	int size of data added
 */
void sensorSettingsUpdate(uint8_t* payload, sensor_config_t* config);

/*sensorInit
 *Sets all the settings for the sensor
 *Sets System time to defined time
 *Must be called before function sensorProcess
 *Inputs:	sensor_config_t* config) -> pointer to configuration structure
 *Returns:	none
 */
void sensorInit(sensor_config_t* config);

/*configMessage
 *adds all defined components to the message
 *Inputs:	uint8_t* message -> pointer to payload string
 *Returns:	uint i --> size of message
 */
uint8_t configMessage(uint8_t* message);




