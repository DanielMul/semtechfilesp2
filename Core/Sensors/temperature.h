#include "adc.h"

//#define SENSORTYPE 		0		//0 = analog, 1 = digital

#define MAX_T_DELTA		0.5		//maximum temperature delta without sending data
#define U_SENSOR_IN		4096	//Voltage of the sensor
#define ADC_REF_VOLTAGE 3.3		//Reference voltage for adc
#define SENSOR_VOLTAGE	3.3		//total voltage over the sensor and the reference resistor
#define R_REF_OHM		2550	//reference resistor value in ohm
#define R_PT1000_ZERO	1000	//PT1000 resistance at 0 degree Celsius

extern Adc_t Adc1;	//adc handle

/*inverseVoltageDivider
 *Performs an inverse voltage divider
 *Inputs:	int Resistance 1, int Voltage 1, int voltage 2
 *Returns:	float Resistance 2
 */
float inverseVoltageDivider(int R1, int U1, int Utotal);

/*getTemp
 *get the temperature from adc 1 channel 1 (PIN PC1)
 *Inputs:	none
 *Returns:	float temperature
 */
float getTemp(void);



