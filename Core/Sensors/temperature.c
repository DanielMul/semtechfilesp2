#include "temperature.h"
#include "delay.h"
#include <math.h>
#include <stdlib.h>
#include <board-config.h>
#include "board.h"


/*inverseVoltageDivider
 *Performs an inverse voltage divider
 *Inputs:	int Resistance 1, int Voltage 1, int voltage 2
 *Returns:	float Resistance 2
 */
float inverseVoltageDivider(int R1, int U1, int Utotal)
{
	//Reverse voltage divider to get resistance
	//function is a little inaccurate because it does not use floats
	return (((float)Utotal/ U1)* R1) - R1;		//reversed voltage divider
}

/*getTemp
 *get the temperature from adc 1 channel 1 (PIN PC1)
 *Inputs:	none
 *Returns:	float temperature
 */
float getTemp(void)
{
	//get voltage of known resistor
	uint8_t i = 0;
	int u_r1 =0;
	while (i++ < 10)
	{
		u_r1 += AdcReadChannel(&Adc1, PT1000_ADC_CHANNEL);
	}
	u_r1 /= i-1;
	float scope = (float)u_r1/(4096)*ADC_REF_VOLTAGE;
	printf("UR_REF:\t%f\r\n", scope);
	printf("UR_PT1000:\t%f\r\n", (SENSOR_VOLTAGE - scope));
	printf("ADC:%i\r\n", (u_r1));

	//Calculate NTC resistance value NTC could also be PT1000!
	float r_pt1000 = inverseVoltageDivider(R_REF_OHM, u_r1, U_SENSOR_IN);
	printf("\t->ohm:%f\r\n", r_pt1000);


	return (r_pt1000 - R_PT1000_ZERO)/ 3.85;
}
