#include <stdio.h>
#include <systime.h>
#include <utilities.h>
#include <stdint.h>
#include <temperature.h>
#include <sensor.h>
#include "delay.h"
#include "math.h"

#define ATTEMPT_FORMAT0
#define TIMEFORMAT0
#define TEMPERATURE_FORMAT0
#define BATTERY_FORMAT0
//#define DUMMY_DATA
float fake_temp[21] = { 20, 25, 25.5, 25.55, 25.53, 25.55, 25.53, 25.55, 25.53, 25.55, 25.6, 40, 37, 38.8, 12, 15, 18, 18.03, 18.05, 18.09, 18.1};
#define FAKE_CNT 21

#ifdef ATTEMPT_FORMAT0
uint8_t attempts;
#endif

uint8_t messageFormat[55];
float	last_t;
uint32_t timestamp_lastmsg;



uint8_t getAttempts(uint8_t *dest);
uint8_t getBattLevel(uint8_t *dest);
uint8_t getSysTime(uint8_t *dest);
uint8_t getTemperature(uint8_t *dest);

#ifdef DUMMY_DATA
uint8_t fakeTi;
#endif

/*sensorProcess
 * Main process for the sensor gets data and checks if it needs to be sent
 *Gets the sensorvalue translates it to temp
 *Checks last time message sent
 *Checks last sent Temperature
 *Inputs:	sensor_config_t* config) -> pointer to configuration structure
 *Returns:	1 or 0 depinding on last time and Temp
 */
uint8_t sensorProcess(sensor_config_t* sensor)
{
	uint8_t temp_changed = 0;

#ifndef DUMMY_DATA
	float current_temp = getTemp() + sensor->temp_offset;
#else
	float current_temp = fake_temp[fakeTi++] + sensor->temp_offset;
	if (fakeTi > FAKE_CNT) fakeTi = 0;
#endif

	float difference = current_temp - last_t;
	printf("Temperature: %f\t->", current_temp);
	//printf("Temperature: after: %i,%i\n\r", ((current_temp >> 2)+10), ((current_temp &0x3)*25));
	printf("difference : %f\n\r", difference);

	double time_since_last = SysTimeGetSec()- (timestamp_lastmsg); // in sec
	printf("seconds since last : %i\n\r", time_since_last);
	if(fabsf(difference) > MAX_T_DELTA)
	{
		//Delta to big send temp
		printf("delta too high send temp");
		temp_changed = 1;
	}
	else
	{
		if ( (time_since_last*1000)> sensor->max_sample_time)	//Too long since last message send message now
		{
			printf("waited to long");
			temp_changed = 1;
		}
	}

	if(temp_changed == 1)
	{
	last_t = current_temp;
	timestamp_lastmsg = SysTimeGetSec();
	}
	return temp_changed;
}

/*configMessage
 *adds all defined components to the message
 *Inputs:	uint8_t* message -> pointer to payload string
 *Returns:	uint i --> size of message
 */
uint8_t configMessage(uint8_t* message)
{
#ifdef EXAMPLE_FORMAT0
	i+= getEXAMPLE_DATA(&message[i]); 	//function returns the size of example data
										//So next data is inserted after the example data
#endif
	uint8_t i = 0;
#ifdef ATTEMPT_FORMAT0
	i+= getAttempts(&message[i]);
#endif
#ifdef TIMEFORMAT0
	i+= getSysTime(&message[i]);
#endif
#ifdef BATTERY_FORMAT0
	i+= getBattLevel(&message[i]);
#endif
#ifdef TEMPERATURE_FORMAT0
	i+= getTemperature(&message[i]);
#endif

	return i;							//returns the size of the payload
}

#ifdef EXEMPLE_FORMAT0
uint8_t getAttempts(uint8_t *dest)
{
	att
	memcpy1(dest, &attempts, 1);
	return 1;
}
#endif

#ifdef ATTEMPT_FORMAT0
/*getAttempts
 *Adds the Attempt cnt to the format
 *Inputs:	uint8_t *dest -> pointer to place in payloads tring
 *Returns:	int size of data added
 */
uint8_t getAttempts(uint8_t *dest)
{
	if (attempts > 255)
	{
	attempts = 1;
	}
	memcpy1(dest, &attempts, 1);
	return 1;
}
#endif

#ifdef BATTERY_FORMAT0
/*getBattLevel
 *Adds the Battery level to the format
 *Inputs:	uint8_t *dest -> pointer to place in payloads tring
 *Returns:	int size of data added
 */
uint8_t getBattLevel(uint8_t *dest)
{
	//dummydata
	uint8_t batteryLvl = 0x25;
	memcpy1(dest, &batteryLvl, 1);
	return 1;
}
#endif

#ifdef TIMEFORMAT0
/*getSysTime
 *Adds the time to the format
 *Inputs:	uint8_t *dest -> pointer to place in payloads tring
 *Returns:	int size of data added
 */
uint8_t getSysTime(uint8_t *dest)
{
	SysTime_t currentTime = SysTimeGet();
	memcpy1(dest, (uint8_t*)&currentTime.Seconds, sizeof(currentTime.Seconds));
	return sizeof(currentTime.Seconds);
}
#endif

#ifdef TEMPERATURE_FORMAT0
/*getTemperature
 *Adds the temperature to the format
 *Inputs:	uint8_t *dest -> pointer to place in payloads tring
 *Returns:	int size of data added
 */
uint8_t getTemperature(uint8_t *dest)
{
	uint8_t temp[2] = {0x2, 0x1};

	temp[0] = ((int) (last_t *100) + 27315)>> 8;
	temp[1] = ((int) (last_t *100) + 27315);

	memcpyr(dest, temp, sizeof(temp));
	return sizeof(temp);
}
#endif

/*sensorInit
 *Sets all the settings for the sensor
 *Sets System time to defined time
 *Must be called before function sensorProcess
 *Inputs:	sensor_config_t* config) -> pointer to configuration structure
 *Returns:	none
 */
void sensorInit(sensor_config_t* config)
{
	config->sample_time = STANDARD_SAMPLE_TIME;
	config->temp_offset = STANDARD_TEMP_OFFSET;
	config->max_sample_time =STANDARD_MAX_SAMPLE_TIME;

	SysTime_t start_time;
	start_time.Seconds = STANDARD_SYSTEM_TIMESTAMP;
	SysTimeSet(start_time);

	printf("sample_time= %i\n\r",config->sample_time);
	printf("max_sample_time= %i\n\r",config->max_sample_time);
	printf("TEMP OFFSET= %f\n\r",config->temp_offset);
	return;
}

/*sensorSettingsUpdate
 *Adds the temperature to the format
 *Inputs:	uint8_t *dest -> pointer to place in payloads tring
 *Returns:	int size of data added
 */
void sensorSettingsUpdate(uint8_t* payload, sensor_config_t* config)
{
	uint8_t i = 1;
	uint8_t action = payload[0];

	int new_sample_time = 0;
	int new_max_sample_time = 0;
	float new_temp_offset = 0;

	if ((action & sample_time_1s ) == sample_time_1s)
	{
		new_sample_time += payload[i++] * 1000;
	}
	if ((action & sample_time_30s) == sample_time_30s)
	{
		new_sample_time += payload[i++] * 30000;
	}
	if ((action & sample_time_5m ) == sample_time_5m)
	{
		new_sample_time += payload[i++] *300000;
	}
	if (new_sample_time>0)
		{
			config->sample_time = new_sample_time;
		}

	if ((action & sample_offset_50mK) == sample_offset_50mK)
	{
		new_temp_offset += (payload[i++]- 128) *0.050;
	}
	if ((action & sample_offset_1K) == sample_offset_1K)
	{
		new_temp_offset += (payload[i++] - 128) *1;
	}
	if (new_temp_offset>0)
	{
		config->temp_offset = new_temp_offset;
	}

	if ((action & sample_time_max_30s) == sample_time_max_30s)
	{
		new_max_sample_time += payload[i++] * 30000;
	}
	if ((action & sample_time_max_5m) == sample_time_max_5m)
	{
		new_max_sample_time += payload[i++] * 300000;
	}
	if (new_max_sample_time>0)
	{
		config->max_sample_time = new_max_sample_time;
	}


	if ((action & update_system_time) == update_system_time)
	{
		SysTime_t new_time;
		new_time.Seconds = 0;
		uint8_t x = 3;
		while (x>=0)
		{
			new_time.Seconds += payload[i++] << (8*x);
			x--;
		}
		SysTimeSet(new_time);
	}

	printf("sample_time= %i\n\r",config->sample_time);
	printf("max_sample_time= %i\n\r",config->max_sample_time);
	printf("TEMP OFFSET= %f\n\r",config->temp_offset);

	return;
}


