/**
  ******************************************************************************
  * @file    stsafea_service_interface_template.c
  * @author  SMD/AME application teams
  * @version V3.3.0
  * @brief   Service Interface file to support the hardware services required by the
  *          STSAFE-A Middleware and offered by the specific HW, Low Level library
  *          selected by the user. E.g.:
  *           + IOs
  *           + Communication Bus (e.g. I2C)
  *           + Timing delay
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * STSAFE DRIVER SOFTWARE LICENSE AGREEMENT (SLA0088)
  *
  * BY INSTALLING, COPYING, DOWNLOADING, ACCESSING OR OTHERWISE USING THIS SOFTWARE
  * OR ANY PART THEREOF (AND THE RELATED DOCUMENTATION) FROM STMICROELECTRONICS
  * INTERNATIONAL N.V, SWISS BRANCH AND/OR ITS AFFILIATED COMPANIES (STMICROELECTRONICS),
  * THE RECIPIENT, ON BEHALF OF HIMSELF OR HERSELF, OR ON BEHALF OF ANY ENTITY BY WHICH
  * SUCH RECIPIENT IS EMPLOYED AND/OR ENGAGED AGREES TO BE BOUND BY THIS SOFTWARE LICENSE
  * AGREEMENT.
  *
  * Under STMicroelectronics� intellectual property rights, the redistribution,
  * reproduction and use in source and binary forms of the software or any part thereof,
  * with or without modification, are permitted provided that the following conditions
  * are met:
  * 1.  Redistribution of source code (modified or not) must retain any copyright notice,
  *     this list of conditions and the disclaimer set forth below as items 10 and 11.
  * 2.  Redistributions in binary form, except as embedded into a microcontroller or
  *     microprocessor device or a software update for such device, must reproduce any
  *     copyright notice provided with the binary code, this list of conditions, and the
  *     disclaimer set forth below as items 10 and 11, in documentation and/or other
  *     materials provided with the distribution.
  * 3.  Neither the name of STMicroelectronics nor the names of other contributors to this
  *     software may be used to endorse or promote products derived from this software or
  *     part thereof without specific written permission.
  * 4.  This software or any part thereof, including modifications and/or derivative works
  *     of this software, must be used and execute solely and exclusively in combination
  *     with a secure microcontroller device from STSAFE family manufactured by or for
  *     STMicroelectronics.
  * 5.  No use, reproduction or redistribution of this software partially or totally may be
  *     done in any manner that would subject this software to any Open Source Terms.
  *     �Open Source Terms� shall mean any open source license which requires as part of
  *     distribution of software that the source code of such software is distributed
  *     therewith or otherwise made available, or open source license that substantially
  *     complies with the Open Source definition specified at www.opensource.org and any
  *     other comparable open source license such as for example GNU General Public
  *     License(GPL), Eclipse Public License (EPL), Apache Software License, BSD license
  *     or MIT license.
  * 6.  STMicroelectronics has no obligation to provide any maintenance, support or
  *     updates for the software.
  * 7.  The software is and will remain the exclusive property of STMicroelectronics and
  *     its licensors. The recipient will not take any action that jeopardizes
  *     STMicroelectronics and its licensors' proprietary rights or acquire any rights
  *     in the software, except the limited rights specified hereunder.
  * 8.  The recipient shall comply with all applicable laws and regulations affecting the
  *     use of the software or any part thereof including any applicable export control
  *     law or regulation.
  * 9.  Redistribution and use of this software or any part thereof other than as  permitted
  *     under this license is void and will automatically terminate your rights under this
  *     license.
  * 10. THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" AND ANY
  *     EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  *     WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
  *     OF THIRD PARTY INTELLECTUAL PROPERTY RIGHTS, WHICH ARE DISCLAIMED TO THE FULLEST
  *     EXTENT PERMITTED BY LAW. IN NO EVENT SHALL STMICROELECTRONICS OR CONTRIBUTORS BE
  *     LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  *     THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  *     NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  *     ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  * 11. EXCEPT AS EXPRESSLY PERMITTED HEREUNDER, NO LICENSE OR OTHER RIGHTS, WHETHER EXPRESS
  *     OR IMPLIED, ARE GRANTED UNDER ANY PATENT OR OTHER INTELLECTUAL PROPERTY RIGHTS OF
  *     STMICROELECTRONICS OR ANY THIRD PARTY.
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "stsafea_service.h"
#include "stsafea_interface_conf.h"
#include "i2c.h"
#include "board.h"
#include "board-config.h"
#include "delay.h"
#include MCU_PLATFORM_BUS_INCLUDE
//#include MCU_PLATFORM_CRC_INCLUDE
//#include MCU_PLATFORM_INCLUDE

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define STM32_DEVICE_ADRESS						0x0000
#define STSAFEA_DEVICE_ADDRESS                  0x0020

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
int myglobal =0;
I2c_t I2c1;

//  const struct Radio_s Radio =/
I2cId_t I2C_ID = I2C_1;

/* Private function prototypes -----------------------------------------------*/
int32_t StSafe_HW_IOInit(void);
int32_t StSafe_I2cInit(void);
int32_t StSafe_I2cDeInit(void);
int32_t StSafe_I2cSend(uint16_t address, uint8_t *pbuffer, uint16_t size);
int32_t StSafe_I2cRecv (uint16_t address, uint8_t *pbuffer, uint16_t size);
int32_t StSafe_CrcInit(void);
uint32_t StSafe_Crc_Compute(uint8_t *pData1, uint16_t Length1, uint8_t *pData2, uint16_t Length2);

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Configure STSAFE IO and Bus operation functions to be implemented at User level
  * @param  Ctx the STSAFE IO context
  * @retval 0 in case of success, an error code otherwise
  */


int8_t StSafeA_HW_Probe(void *pCtx)
{
  STSAFEA_HW_t *HwCtx = (STSAFEA_HW_t *)pCtx;


  HwCtx->IOInit     = StSafe_HW_IOInit;
  HwCtx->BusInit    = StSafe_I2cInit;
  HwCtx->BusDeInit  = StSafe_I2cDeInit;
  HwCtx->BusSend    = StSafe_I2cSend;
  HwCtx->BusRecv    = StSafe_I2cRecv;
  HwCtx->CrcInit    = StSafe_CrcInit;
  HwCtx->CrcCompute = StSafe_Crc_Compute;
  HwCtx->TimeDelay  = DelayMs;
  HwCtx->DevAddr    = STSAFEA_DEVICE_ADDRESS;

  return STSAFEA_HW_OK;
}

int32_t StSafe_HW_IOInit(void)
{
  /* Initialize the STSAFE-A  IO */
	return 0;
}

int32_t StSafe_I2cInit(void)
{
//	  Init bus with Hal or I2c.c
  I2cInit( &I2c1, I2C_ID, I2C_SCL, I2C_SDA );
  return 0;
}

int32_t StSafe_I2cDeInit(void)
{
  //	  deInit bus with Hal or I2c.c
  I2cDeInit( &I2c1);
  return 0;
}

int32_t StSafe_I2cSend(uint16_t address, uint8_t *pbuffer, uint16_t size)
{
//	HAL_I2C_IsDeviceReady(&I2c1, address, 5, 500);
  I2cWriteBuffer(&I2c1, address, STM32_DEVICE_ADRESS, pbuffer, size);
  return 0;
  //return error code?
}

int32_t StSafe_I2cRecv (uint16_t address, uint8_t *pbuffer, uint16_t size)
{
  I2cReadBuffer(&I2c1, address, STM32_DEVICE_ADRESS, pbuffer, size);
  return 0;
}



int32_t StSafe_CrcInit(void)
{
//	  Needs work
  return 0;
}

uint32_t StSafe_Crc_Compute(uint8_t *pData1, uint16_t Length1, uint8_t *pData2, uint16_t Length2)
{
  return 0; //need work
}

uint16_t DevAddr;

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
