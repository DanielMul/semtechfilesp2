/**
  ******************************************************************************
  * @file    authentication.c
  * @author  SMD application team
  * @version V3.3.0
  * @brief   Authentication use case using STSAFE-A and STM32 Cube cryptographic library.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>

#ifdef HAL_UART_MODULE_ENABLED
#include <stdio.h>
#endif /* HAL_UART_MODULE_ENABLED */

#include "stsafea_interface_conf.h"
#ifdef MCU_PLATFORM_INCLUDE
#include MCU_PLATFORM_INCLUDE
#endif /* MCU_PLATFORM_INCLUDE */

#include "stsafea_core.h"

#include "x509.h"
#include "x509_subparsing.h"
#include "x509_crypto.h"


#define STS_CHK(ret, f)                     if ((ret) == 0) { (ret) = (f); }

#define GET_TICK()                          HAL_GetTick()

int32_t authentication(StSafeA_Handle_t* handle);
static uint8_t GenerateUnsignedChallenge(StSafeA_Handle_t* handle, uint8_t size, uint8_t* random);
static int32_t GetCertificateSize(StSafeA_Handle_t* handle, uint16_t *Certificate_Size);
static int32_t ExtractParseVerifyCertificate(StSafeA_Handle_t* handle, StSafeA_LVBuffer_t *raw_cert, intCert_stt* STS_Certificate);
static int32_t PeripheralAuthentication(StSafeA_Handle_t* handle, intCert_stt* STS_Certificate);

#ifdef HAL_UART_MODULE_ENABLED
static uint8_t idx = 0;
#endif /* HAL_UART_MODULE_ENABLED */


/* Root CA key used for engineering sample */
#define CA_SELF_SIGNED_CERTIFICATE_DEMO \
  0x30,0x82,0x01,0x94,0x30,0x82,0x01,0x3A,0xA0,0x03,0x02,0x01,0x02,0x02,0x01,0x01, \
  0x30,0x09,0x06,0x07,0x2A,0x86,0x48,0xCE,0x3D,0x04,0x01,0x30,0x54,0x31,0x0B,0x30, \
  0x09,0x06,0x03,0x55,0x04,0x06,0x13,0x02,0x4E,0x4C,0x31,0x1E,0x30,0x1C,0x06,0x03, \
  0x55,0x04,0x0A,0x0C,0x15,0x53,0x54,0x4D,0x69,0x63,0x72,0x6F,0x65,0x6C,0x65,0x63, \
  0x74,0x72,0x6F,0x6E,0x69,0x63,0x73,0x20,0x6E,0x76,0x31,0x25,0x30,0x23,0x06,0x03, \
  0x55,0x04,0x03,0x0C,0x1C,0x53,0x54,0x4D,0x20,0x43,0x41,0x20,0x44,0x65,0x6D,0x6F, \
  0x20,0x49,0x6E,0x74,0x65,0x72,0x6D,0x65,0x64,0x69,0x61,0x74,0x65,0x20,0x4B,0x65, \
  0x79,0x30,0x1E,0x17,0x0D,0x31,0x36,0x30,0x33,0x30,0x34,0x30,0x30,0x30,0x30,0x30, \
  0x30,0x5A,0x17,0x0D,0x34,0x36,0x30,0x33,0x30,0x34,0x30,0x30,0x30,0x30,0x30,0x30, \
  0x5A,0x30,0x54,0x31,0x0B,0x30,0x09,0x06,0x03,0x55,0x04,0x06,0x13,0x02,0x4E,0x4C, \
  0x31,0x1E,0x30,0x1C,0x06,0x03,0x55,0x04,0x0A,0x0C,0x15,0x53,0x54,0x4D,0x69,0x63, \
  0x72,0x6F,0x65,0x6C,0x65,0x63,0x74,0x72,0x6F,0x6E,0x69,0x63,0x73,0x20,0x6E,0x76, \
  0x31,0x25,0x30,0x23,0x06,0x03,0x55,0x04,0x03,0x0C,0x1C,0x53,0x54,0x4D,0x20,0x43, \
  0x41,0x20,0x44,0x65,0x6D,0x6F,0x20,0x49,0x6E,0x74,0x65,0x72,0x6D,0x65,0x64,0x69, \
  0x61,0x74,0x65,0x20,0x4B,0x65,0x79,0x30,0x59,0x30,0x13,0x06,0x07,0x2A,0x86,0x48, \
  0xCE,0x3D,0x02,0x01,0x06,0x08,0x2A,0x86,0x48,0xCE,0x3D,0x03,0x01,0x07,0x03,0x42, \
  0x00,0x04,0x27,0x21,0xC5,0x62,0xF8,0xF2,0x26,0x8F,0xAD,0xD3,0x74,0x1B,0xC5,0x8B, \
  0x98,0xF4,0x16,0xC2,0x21,0x7A,0xB9,0x8B,0xF0,0xDE,0xBF,0x79,0x2D,0xB2,0xC7,0x2F, \
  0xC2,0xFF,0xA6,0x1C,0xA3,0x26,0x38,0x0C,0xD3,0x5D,0x77,0xDB,0x39,0x69,0x76,0x36, \
  0x4A,0x23,0x68,0xC5,0x65,0xF9,0x69,0x21,0xBF,0xA1,0xCC,0x3B,0x2A,0x62,0x1F,0xAD, \
  0x62,0xB9,0x30,0x09,0x06,0x07,0x2A,0x86,0x48,0xCE,0x3D,0x04,0x01,0x03,0x49,0x00, \
  0x30,0x46,0x02,0x21,0x00,0x98,0xDD,0x31,0xD7,0x12,0x25,0xC6,0x6D,0x86,0x86,0x72, \
  0x6E,0xB0,0x98,0x6E,0x86,0x12,0x9C,0xF8,0xAD,0x55,0x29,0x7F,0xA8,0xC2,0xA8,0x24, \
  0xD9,0xBF,0xAA,0x72,0x6D,0x02,0x21,0x00,0x9F,0x48,0x18,0xA7,0x1C,0xFB,0xED,0x8B, \
  0x18,0x82,0xAC,0x0E,0xD5,0xAD,0xE8,0x64,0xE1,0x45,0xC8,0xA6,0xAE,0xFC,0x56,0xBC, \
  0x1D,0x10,0xC3,0x6C,0xA5,0xB4,0x0C,0xE9


/* Root CA key used for SPL1 */
#define CA_SELF_SIGNED_CERTIFICATE_91 \
  0x30,0x82,0x01,0x9D,0x30,0x82,0x01,0x45,0xA0,0x03,0x02,0x01,0x02,0x02,0x01,0x01, \
  0x30,0x09,0x06,0x07,0x2A,0x86,0x48,0xCE,0x3D,0x04,0x01,0x30,0x4F,0x31,0x0B,0x30, \
  0x09,0x06,0x03,0x55,0x04,0x06,0x13,0x02,0x4E,0x4C,0x31,0x1E,0x30,0x1C,0x06,0x03, \
  0x55,0x04,0x0A,0x0C,0x15,0x53,0x54,0x4D,0x69,0x63,0x72,0x6F,0x65,0x6C,0x65,0x63, \
  0x74,0x72,0x6F,0x6E,0x69,0x63,0x73,0x20,0x6E,0x76,0x31,0x20,0x30,0x1E,0x06,0x03, \
  0x55,0x04,0x03,0x0C,0x17,0x53,0x54,0x4D,0x20,0x53,0x54,0x53,0x41,0x46,0x45,0x2D, \
  0x41,0x20,0x54,0x45,0x53,0x54,0x20,0x43,0x41,0x20,0x39,0x31,0x30,0x1E,0x17,0x0D, \
  0x31,0x37,0x30,0x32,0x30,0x31,0x30,0x30,0x30,0x30,0x30,0x30,0x5A,0x17,0x0D,0x34, \
  0x37,0x30,0x32,0x30,0x31,0x30,0x30,0x30,0x30,0x30,0x30,0x5A,0x30,0x4F,0x31,0x0B, \
  0x30,0x09,0x06,0x03,0x55,0x04,0x06,0x13,0x02,0x4E,0x4C,0x31,0x1E,0x30,0x1C,0x06, \
  0x03,0x55,0x04,0x0A,0x0C,0x15,0x53,0x54,0x4D,0x69,0x63,0x72,0x6F,0x65,0x6C,0x65, \
  0x63,0x74,0x72,0x6F,0x6E,0x69,0x63,0x73,0x20,0x6E,0x76,0x31,0x20,0x30,0x1E,0x06, \
  0x03,0x55,0x04,0x03,0x0C,0x17,0x53,0x54,0x4D,0x20,0x53,0x54,0x53,0x41,0x46,0x45, \
  0x2D,0x41,0x20,0x54,0x45,0x53,0x54,0x20,0x43,0x41,0x20,0x39,0x31,0x30,0x59,0x30, \
  0x13,0x06,0x07,0x2A,0x86,0x48,0xCE,0x3D,0x02,0x01,0x06,0x08,0x2A,0x86,0x48,0xCE, \
  0x3D,0x03,0x01,0x07,0x03,0x42,0x00,0x04,0x27,0x21,0xC5,0x62,0xF8,0xF2,0x26,0x8F, \
  0xAD,0xD3,0x74,0x1B,0xC5,0x8B,0x98,0xF4,0x16,0xC2,0x21,0x7A,0xB9,0x8B,0xF0,0xDE, \
  0xBF,0x79,0x2D,0xB2,0xC7,0x2F,0xC2,0xFF,0xA6,0x1C,0xA3,0x26,0x38,0x0C,0xD3,0x5D, \
  0x77,0xDB,0x39,0x69,0x76,0x36,0x4A,0x23,0x68,0xC5,0x65,0xF9,0x69,0x21,0xBF,0xA1, \
  0xCC,0x3B,0x2A,0x62,0x1F,0xAD,0x62,0xB9,0xA3,0x13,0x30,0x11,0x30,0x0F,0x06,0x03, \
  0x55,0x1D,0x13,0x01,0x01,0xFF,0x04,0x05,0x30,0x03,0x01,0x01,0xFF,0x30,0x09,0x06, \
  0x07,0x2A,0x86,0x48,0xCE,0x3D,0x04,0x01,0x03,0x47,0x00,0x30,0x44,0x02,0x20,0x64, \
  0xCA,0xB2,0x45,0xF5,0x7A,0x57,0x0B,0xCE,0x39,0x8B,0x85,0x85,0x57,0x55,0x60,0x6F, \
  0x92,0xA5,0xB7,0x9F,0x28,0x1F,0xD6,0x3C,0xBF,0x7D,0xBE,0x00,0xEA,0xE3,0x3E,0x02, \
  0x20,0x1A,0x88,0xCB,0xFC,0xDC,0x04,0x10,0xC9,0x74,0x48,0x73,0x1E,0xDB,0xB5,0x31, \
  0xC7,0xDC,0x17,0xB8,0x77,0xD2,0x78,0xD2,0x7B,0xBC,0x0F,0x71,0x69,0x4F,0x7A,0xFD, \
  0x7C

/* Root CA key used for SPL2 */
#define CA_SELF_SIGNED_CERTIFICATE_01 \
  0x30,0x82,0x01,0xA0,0x30,0x82,0x01,0x46,0xA0,0x03,0x02,0x01,0x02,0x02,0x01,0x01, \
  0x30,0x0A,0x06,0x08,0x2A,0x86,0x48,0xCE,0x3D,0x04,0x03,0x02,0x30,0x4F,0x31,0x0B, \
  0x30,0x09,0x06,0x03,0x55,0x04,0x06,0x13,0x02,0x4E,0x4C,0x31,0x1E,0x30,0x1C,0x06, \
  0x03,0x55,0x04,0x0A,0x0C,0x15,0x53,0x54,0x4D,0x69,0x63,0x72,0x6F,0x65,0x6C,0x65, \
  0x63,0x74,0x72,0x6F,0x6E,0x69,0x63,0x73,0x20,0x6E,0x76,0x31,0x20,0x30,0x1E,0x06, \
  0x03,0x55,0x04,0x03,0x0C,0x17,0x53,0x54,0x4D,0x20,0x53,0x54,0x53,0x41,0x46,0x45, \
  0x2D,0x41,0x20,0x50,0x52,0x4F,0x44,0x20,0x43,0x41,0x20,0x30,0x31,0x30,0x1E,0x17, \
  0x0D,0x31,0x38,0x30,0x37,0x32,0x37,0x30,0x30,0x30,0x30,0x30,0x30,0x5A,0x17,0x0D, \
  0x34,0x38,0x30,0x37,0x32,0x37,0x30,0x30,0x30,0x30,0x30,0x30,0x5A,0x30,0x4F,0x31, \
  0x0B,0x30,0x09,0x06,0x03,0x55,0x04,0x06,0x13,0x02,0x4E,0x4C,0x31,0x1E,0x30,0x1C, \
  0x06,0x03,0x55,0x04,0x0A,0x0C,0x15,0x53,0x54,0x4D,0x69,0x63,0x72,0x6F,0x65,0x6C, \
  0x65,0x63,0x74,0x72,0x6F,0x6E,0x69,0x63,0x73,0x20,0x6E,0x76,0x31,0x20,0x30,0x1E, \
  0x06,0x03,0x55,0x04,0x03,0x0C,0x17,0x53,0x54,0x4D,0x20,0x53,0x54,0x53,0x41,0x46, \
  0x45,0x2D,0x41,0x20,0x50,0x52,0x4F,0x44,0x20,0x43,0x41,0x20,0x30,0x31,0x30,0x59, \
  0x30,0x13,0x06,0x07,0x2A,0x86,0x48,0xCE,0x3D,0x02,0x01,0x06,0x08,0x2A,0x86,0x48, \
  0xCE,0x3D,0x03,0x01,0x07,0x03,0x42,0x00,0x04,0x82,0x19,0x4F,0x26,0xCC,0xA3,0x6E, \
  0x0E,0x82,0x19,0x5C,0xE6,0x66,0x58,0xEC,0x64,0xA4,0x66,0x92,0x2F,0x58,0xC9,0xE6, \
  0x4B,0x5D,0xE1,0xA2,0x9E,0x7F,0x39,0x86,0x3D,0x04,0x26,0x92,0xE4,0xC8,0xAC,0x79, \
  0xF9,0x6D,0x2F,0xED,0x52,0x77,0x4D,0x52,0x81,0x95,0x39,0xF2,0x1F,0x3E,0xCD,0x19, \
  0x38,0xF8,0x3D,0x70,0xAE,0xE0,0x9C,0xCD,0x8D,0xA3,0x13,0x30,0x11,0x30,0x0F,0x06, \
  0x03,0x55,0x1D,0x13,0x01,0x01,0xFF,0x04,0x05,0x30,0x03,0x01,0x01,0xFF,0x30,0x0A, \
  0x06,0x08,0x2A,0x86,0x48,0xCE,0x3D,0x04,0x03,0x02,0x03,0x48,0x00,0x30,0x45,0x02, \
  0x20,0x6E,0xE5,0x43,0x32,0x47,0xAC,0x72,0x34,0xFC,0x9D,0x17,0x5A,0xA5,0x1E,0x83, \
  0x27,0x69,0x01,0xAD,0xEC,0x1F,0x00,0x5E,0x37,0x1F,0x40,0x73,0x4D,0xE3,0x8C,0xC5, \
  0x2E,0x02,0x21,0x00,0xB1,0xD9,0x51,0x6A,0xAD,0x9A,0x3E,0x86,0xD2,0x2B,0x8E,0x3B, \
  0x3B,0xD0,0x14,0x6F,0xAB,0xB9,0xB9,0x22,0xF0,0x45,0x26,0x34,0xFE,0x92,0x7F,0xF5, \
  0xD6,0x36,0xCD,0x90


#define NUMBER_OF_BYTES_TO_GET_CERTIFICATE_SIZE              4

/************************ Generate Unsigned Bytes Array ************************/
static uint8_t GenerateUnsignedChallenge(StSafeA_Handle_t* handle, uint8_t size, uint8_t* random)
{
  if (random == NULL)
  {
    return (1);
  }

#ifdef HAL_UART_MODULE_ENABLED
  printf("\n\r %d. Generate a %d bytes random number", ++idx, size);
  printf("\n\r    => Use StSafeA_GenerateRandom API");
#endif /* HAL_UART_MODULE_ENABLED */

  StSafeA_LVBuffer_t TrueRandom;
  TrueRandom.Data = random;
  return ((uint8_t)StSafeA_GenerateRandom(handle, STSAFEA_EPHEMERAL_RND, size, &TrueRandom, STSAFEA_MAC_NONE));
}


/************************ ExtractParseVerifyCertificate ************************/
static int32_t GetCertificateSize(StSafeA_Handle_t* handle, uint16_t *Certificate_Size)
{
  int32_t                       StatusCode = 0;

  StSafeA_LVBuffer_t sts_read;
#if (!STSAFEA_USE_OPTIMIZATION_SHARED_RAM)
  uint8_t data_sts_read [NUMBER_OF_BYTES_TO_GET_CERTIFICATE_SIZE];
  sts_read.Length = NUMBER_OF_BYTES_TO_GET_CERTIFICATE_SIZE;
  sts_read.Data = data_sts_read;
#endif

#ifdef HAL_UART_MODULE_ENABLED
  printf("\n\r %d. Get size of certificate stored through STSAFE-A's zone 0", ++idx);
  printf("\n\r    %d.1 Read %d bytes of certificate through STSAFE-A's zone 0", idx, NUMBER_OF_BYTES_TO_GET_CERTIFICATE_SIZE);
  printf("\n\r        => Use StSafeA_Read API");
#endif /* HAL_UART_MODULE_ENABLED */
  /* Extract 4th 1st bytes of STSAFE-A's X509 CRT certificate */
  STS_CHK(StatusCode, (int32_t)StSafeA_Read(handle, 0, 0, STSAFEA_AC_ALWAYS, 0, 0, NUMBER_OF_BYTES_TO_GET_CERTIFICATE_SIZE, &sts_read, STSAFEA_MAC_NONE));

  if (StatusCode == 0)
  {
    switch (sts_read.Data[1])
    {
      case 0x81U:
        *Certificate_Size = (uint16_t)sts_read.Data[2] + 3U;
        break;

      case 0x82U:
        *Certificate_Size = (((uint16_t)sts_read.Data[2]) << 8) + sts_read.Data[3] + 4U;
        break;

      default:
        if (sts_read.Data[1] < 0x81U)
        {
          *Certificate_Size = sts_read.Data[1];
        }
        break;
    }
#ifdef HAL_UART_MODULE_ENABLED
    printf("\n\r    %d.2 Size of certificate stored through STSAFE-A's zone 0 is %d bytes", idx, *Certificate_Size);
#endif /* HAL_UART_MODULE_ENABLED */
  }

  return (StatusCode);
}

/************************ ExtractParseVerifyCertificate ************************/
static int32_t ExtractParseVerifyCertificate(StSafeA_Handle_t* handle, StSafeA_LVBuffer_t *raw_cert, intCert_stt* STS_Certificate)
{
  int32_t                       StatusCode = 0;
  intCert_stt                   CASelfSignedCert;
  static const uint8_t          CASelfSignedCertificate_SPL2 [] = {CA_SELF_SIGNED_CERTIFICATE_01};

#ifdef HAL_UART_MODULE_ENABLED
  printf("\n\r %d. Extract, parse and verify certificate stored through STSAFE-A's zone 0", ++idx);
  printf("\n\r    %d.1 Read %d bytes through STSAFE-A's zone 0 corresponding to IoT certificate", idx, raw_cert->Length);
  printf("\n\r        => Use StSafeA_Read API");
#endif /* HAL_UART_MODULE_ENABLED */

  /* Extract STSAFE-A's X509 CRT certificate */
  STS_CHK(StatusCode, (int32_t)StSafeA_Read(handle, 0, 0, STSAFEA_AC_ALWAYS, 0, 0, raw_cert->Length, raw_cert, STSAFEA_MAC_NONE));

#ifdef HAL_UART_MODULE_ENABLED
  if (StatusCode == 0U)
  {
    printf("\n\r    %d.2 Parse certificate extracted from STSAFE-A's zone 0", idx);
  }
#endif /* HAL_UART_MODULE_ENABLED */

  /* Parse STSAFE-A's X509 CRT certificate */
  STS_CHK(StatusCode, parseCert(raw_cert->Data, STS_Certificate, NULL));

#ifdef HAL_UART_MODULE_ENABLED
  if (StatusCode == 0U)
  {
    printf("\n\r %d. Parse CA Self Signed certificate", ++idx);
    printf("\n\r %d. Check STSAFE-A's certificate was signed by CA using cryptographic library", ++idx);
  }
#endif /* HAL_UART_MODULE_ENABLED */

  /* Check if root certificate is SPL2 profile */
  /* Parse CA SPL2 certificate */
  STS_CHK(StatusCode, parseCert(CASelfSignedCertificate_SPL2, &CASelfSignedCert , NULL));

  /* Verify STSAFE-A's X509 self signed CRT certificate */
  STS_CHK(StatusCode, isParent(&CASelfSignedCert, STS_Certificate, NULL) == 1 ? 0 : 1);

  /* Check if root certificate is SPL1 profile */
  if (StatusCode == 1)
  {
    static const uint8_t          CASelfSignedCertificate_SPL1 [] = {CA_SELF_SIGNED_CERTIFICATE_91};

    /* Parse CA SPL1 certificate */
    StatusCode = parseCert(CASelfSignedCertificate_SPL1, &CASelfSignedCert , NULL);

    /* Verify STSAFE-A's X509 self signed CRT certificate */
    STS_CHK(StatusCode, (isParent(&CASelfSignedCert, STS_Certificate, NULL) == 1) ? 0 : 1);
  }

  /* Check if root certificate is the one from Engineering Samples */
  if (StatusCode == 1)
  {
    static const uint8_t          CASelfSignedCertificate_DEMO [] = {CA_SELF_SIGNED_CERTIFICATE_DEMO};

    /* Parse CA certificate */
    StatusCode = parseCert(CASelfSignedCertificate_DEMO, &CASelfSignedCert , NULL);

    /* Force the Engineering CA certificate extension because was not set at time of generation (bug in Toolkit) */
    CASelfSignedCert.extensionsFlags = 0x7;

    /* Verify STSAFE-A's X509 self signed CRT certificate */
    STS_CHK(StatusCode, (isParent(&CASelfSignedCert, STS_Certificate, NULL) == 1) ? 0 : 1);
  }

  return (StatusCode);
}


/************************ Peripheral Authentication ************************/
static int32_t PeripheralAuthentication(StSafeA_Handle_t* handle, intCert_stt* STS_Certificate)
{
  int32_t          StatusCode = 0;
  uint8_t          Hash[STSAFEA_GET_HASH_SIZE((uint8_t)STSAFEA_SHA_256)];
  int32_t          Hash_Size = (int32_t)STSAFEA_GET_HASH_SIZE((uint8_t)STSAFEA_SHA_256);
  HASHctx_stt      ctx;
  ECDSAsign_stt    Sign;

  StSafeA_LVBuffer_t SignR, SignS;
#if (!STSAFEA_USE_OPTIMIZATION_SHARED_RAM)
  uint8_t data_SignR [STSAFEA_XYRS_ECDSA_SHA256_LENGTH];
  SignR.Length = STSAFEA_XYRS_ECDSA_SHA256_LENGTH;
  SignR.Data = data_SignR;
  uint8_t data_SignS [STSAFEA_XYRS_ECDSA_SHA256_LENGTH];
  SignS.Length = STSAFEA_XYRS_ECDSA_SHA256_LENGTH;
  SignS.Data = data_SignS;
#endif
  
  /* Generate challenge & hash */
  STS_CHK(StatusCode, (int32_t)GenerateUnsignedChallenge(handle, (uint8_t)sizeof(Hash), Hash));

  ctx.mFlags = E_HASH_DEFAULT;
  ctx.mTagSize = CRL_SHA256_SIZE;
  (void)SHA256_Init(&ctx);
  (void)SHA256_Append(&ctx, Hash, (int32_t)sizeof(Hash));
  (void)SHA256_Finish(&ctx, Hash, &Hash_Size);

#ifdef HAL_UART_MODULE_ENABLED
  if (StatusCode == 0U)
  {
    printf("\n\r %d. Generate signature using STSAFE-A's private key stored into slot 0", ++idx);
    printf("\n\r    => Use StSafeA_GenerateSignature API");
  }
#endif /* HAL_UART_MODULE_ENABLED */

  /* Generate signature of Hash(random) */
  STS_CHK(StatusCode, (int32_t)StSafeA_GenerateSignature(handle, STSAFEA_KEY_SLOT_0, Hash, STSAFEA_SHA_256, 
                                                         STSAFEA_XYRS_ECDSA_SHA256_LENGTH,
                                                         &SignR, &SignS, STSAFEA_MAC_NONE, STSAFEA_ENCRYPTION_NONE));

  /* Verify signature */
  if (StatusCode == 0)
  {
    /* Format Signature */
    Sign.rSize = (int32_t)SignR.Length;
    Sign.pR = SignR.Data;
    Sign.sSize= (int32_t)SignS.Length;
    Sign.pS = SignS.Data;

#ifdef HAL_UART_MODULE_ENABLED
    printf("\n\r %d. Verify the generated signature's validity using cryptographic library with public key of STSAFE-A's slot 0 key pair which was extracted from STSAFE-A's certificate", ++idx);
#endif /* HAL_UART_MODULE_ENABLED */

    /* Verify signature */
    STS_CHK(StatusCode, verifySignature(STS_Certificate, Hash, Hash_Size, Sign.pR, Sign.rSize, Sign.pS, Sign.sSize) == 1 ? 0 : 1);
  }

  return (StatusCode);
}


/***************** Authentication *******************/
int32_t authentication(StSafeA_Handle_t* handle)
{
  int32_t               StatusCode;
  uint16_t              CertSize = 0;
  intCert_stt           STS_Certificate;

#ifdef HAL_UART_MODULE_ENABLED
  idx = 0;
  printf("\n\r\n\rAuthentication demonstration:");
#endif /* HAL_UART_MODULE_ENABLED */

  /* Extract & verify X509 Certificate */
  StatusCode = GetCertificateSize(handle, &CertSize);

  if (StatusCode != 0 || CertSize == 0U)
  {
    return 1;
  }

  /* Store globally the buffer containing certificate */
  StSafeA_LVBuffer_t sts_read_cert;
  sts_read_cert.Length = CertSize;
#if (!STSAFEA_USE_OPTIMIZATION_SHARED_RAM)
  uint8_t data_sts_read_cert [CertSize];
  sts_read_cert.Data = data_sts_read_cert;
#endif

  /* Extract & verify X509 Certificate */
  STS_CHK(StatusCode, ExtractParseVerifyCertificate(handle, &sts_read_cert, &STS_Certificate));

  /* Peripheral Authentication */
  STS_CHK(StatusCode, PeripheralAuthentication(handle, &STS_Certificate));

#ifdef HAL_UART_MODULE_ENABLED
  printf("\n\r %d. Authentication result (0 means success): %d", ++idx, (int)StatusCode);
#endif /* HAL_UART_MODULE_ENABLED */
  return StatusCode;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
