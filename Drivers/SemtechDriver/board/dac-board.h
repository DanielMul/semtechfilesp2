/*!
 * \file      dac-board.h
 *
 * \brief     Target board DAC driver implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#ifndef __DAC_BOARD_H__
#define __DAC_BOARD_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "dac.h"

/*!
 * \brief Initializes the DAC object and MCU peripheral
 *
 * \param [IN] obj      DAC object
 * \param [IN] dacInput DAC output pin
 */
void DacMcuInit( Dac_t *obj, PinNames dacInput );

/*!
 * \brief Initializes the DAC internal parameters
 */
void DacMcuConfig( void );

/*!
 * \brief Reads the value of the given channel
 *
 * \param [IN] obj		DAC object
 * \param [IN] channel	DAC output channel
 * \param [IN] value	DAC output value
 */
void DacMcuSetChannel( Dac_t *obj, uint32_t channel, uint16_t value );

#ifdef __cplusplus
}
#endif

#endif // __DAC_BOARD_H__
