/*!
 * \file      dac-board.c
 *
 * \brief     Target board DAC driver implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include "stm32l5xx.h"
#include "board-config.h"
#include "dac-board.h"

DAC_HandleTypeDef DacHandle;

void DacMcuInit( Dac_t *obj, PinNames dacInput )
{
    DacHandle.Instance = ( DAC_TypeDef* )DAC1_BASE;

    __HAL_RCC_DAC1_CLK_ENABLE( );

    HAL_DAC_DeInit( &DacHandle );

    if( dacInput != NC )
    {
        GpioInit( &obj->DacInput, dacInput, PIN_ANALOGIC, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
    }
}

void DacMcuConfig( void )
{
    // Configure DAC
	  DAC_ChannelConfTypeDef sConfig = {0};

	  /** DAC Initialization
	  */
	  DacHandle.Instance = DAC1;
	  HAL_DAC_Init(&DacHandle);
	  /** DAC channel OUT1 config
	  */
	  sConfig.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
	  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_DISABLE;
	  sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_DISABLE;
	  sConfig.DAC_UserTrimming = DAC_TRIMMING_FACTORY;
	  HAL_DAC_ConfigChannel(&DacHandle, &sConfig, DAC_CHANNEL_1);


}

void DacMcuSetChannel( Dac_t *obj, uint32_t channel, uint16_t value )
{
	  HAL_DAC_Start(&DacHandle, DAC1_CHANNEL_1);
	  HAL_DAC_SetValue(&DacHandle, DAC1_CHANNEL_1, DAC_ALIGN_12B_R, 1000);

}
