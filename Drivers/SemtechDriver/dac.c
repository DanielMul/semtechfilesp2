/*!
 * \file      dac.c
 *
 * \brief     Generic DAC driver implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include <stdbool.h>
#include "dac-board.h"

/*!
 * Flag to indicates if the DAC is initialized
 */
static bool DacInitialized = false;

void DacInit( Dac_t *obj, PinNames dacInput )
{
    if( DacInitialized == false )
    {
        DacInitialized = true;

        DacMcuInit( obj, dacInput );
        DacMcuConfig( );
    }
}

void DacDeInit( Dac_t *obj )
{
    DacInitialized = false;
}

void DacSetChannel( Dac_t *obj, uint32_t channel, uint16_t value )
{

}
