/*!
 * \file      dac.h
 *
 * \brief     Generic DAC driver implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#ifndef __DAC_H__
#define __DAC_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "gpio.h"

/*!
 * DAC object type definition
 */
typedef struct
{
    Gpio_t DacInput;
}Dac_t;

/*!
 * \brief Initializes the DAC input
 *
 * \param [IN] obj  DAC object
 * \param [IN] scl  DAC input pin name to be used
 */
void DacInit( Dac_t *obj, PinNames dacInput );

/*!
 * \brief DeInitializes the DAC
 *
 * \param [IN] obj  DAC object
 */
void DacDeInit( Dac_t *obj );

/*!
 * \brief Read the analogue voltage value
 *
 * \param [IN] obj  DAC object
 * \param [IN] channel DAC channel
 * \param [IN] value DAC value
 */
void DacSetChannel( Dac_t *obj, uint32_t channel, uint16_t value );

#ifdef __cplusplus
}
#endif

#endif // __DAC_H__
